DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'localhost',
        'PORT': 5432,
    }
}

CELERY_BROKER_URL = 'amqp://guest:guest@localhost//'

LOG_LEVEL = "INFO"
