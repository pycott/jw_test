from celery import shared_task
from django.db import transaction
from django.db.models import F

from content_pages.models import Page


@shared_task
def increment_counters_for_pages_contents(page_id):
    page = Page.objects.get(id=page_id)
    with transaction.atomic():
        page.contents.all().update(counter=F('counter') + 1)
