from django import forms
from django.conf.urls import url
from django.contrib import admin

from . import models


class PageContentInline(admin.TabularInline):
    model = models.Page.contents.through
    extra = 1


class ContentAdmin(admin.ModelAdmin):
    readonly_fields = ('counter',)
    search_fields = ('^title',)
    inlines = [
        PageContentInline,
    ]

    def content_type(self, obj):
        return obj._meta.verbose_name

    def get_form(self, request, obj=None, **kwargs):
        if obj is None:
            Model = models.Content.get_subclass(request.GET.get('content_type'))
        else:
            Model = obj.get_child_content().__class__

        RELOAD_PAGE = "window.location.search=window.location.search + '&content_type=' + this.value"

        class ModelForm(forms.ModelForm):
            if not obj:
                content_type = forms.ChoiceField(
                    choices=[('', 'Please select...')] + models.Content.get_subclass_choices(),
                    widget=forms.Select(attrs={'onchange': RELOAD_PAGE})
                )

            class Meta:
                model = Model
                exclude = ("counter",)

        return ModelForm

    def get_fields(self, request, obj=None):
        fields = super(ContentAdmin, self).get_fields(request, obj)

        if 'content_type' in fields:
            fields.remove('content_type')
            fields = ['content_type'] + fields

        return fields

    def get_urls(self):
        urls = super(ContentAdmin, self).get_urls()
        existing = '{}_{}_'.format(self.model._meta.app_label, self.model._meta.model_name)
        subclass_urls = []
        for name, model in models.Content.get_subclass_object_choices().items():
            opts = model._meta
            replace = '{}_{}_'.format(opts.app_label, opts.model_name)

            patterns = []
            for pattern in urls:
                if pattern.name:
                    new_name = pattern.name.replace(existing, replace)
                    url_pattern = url(pattern.pattern.regex.pattern, pattern.callback, name=new_name)
                    patterns.append(url_pattern)

            subclass_urls.extend(patterns)

        return urls + subclass_urls


class PageAdmin(admin.ModelAdmin):
    search_fields = ('^title',)
    inlines = [
        PageContentInline,
    ]
    exclude = ('contents',)


admin.site.register(models.Page, PageAdmin)
admin.site.register(models.Content, ContentAdmin)
