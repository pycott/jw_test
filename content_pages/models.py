from django.db import models


class Page(models.Model):
    title = models.CharField(max_length=255)
    contents = models.ManyToManyField("Content")

    def __str__(self):
        return self.title


class Content(models.Model):
    counter = models.PositiveIntegerField(default=0)
    title = models.CharField(max_length=255)

    def get_child_content(self):
        for subclass in self.get_subclass_object_choices().keys():
            if hasattr(self, subclass):
                return getattr(self, subclass)
        raise ValueError(f"Cannot find child for content with pk {self.pk}")

    def __str__(self):
        return self.title

    @classmethod
    def get_subclass_object_choices(cls):
        subclass_object_choices = {}

        for rel in cls._meta.related_objects:
            if rel.parent_link:
                subclass_object_choices[rel.name] = rel.related_model

        return subclass_object_choices

    @classmethod
    def get_subclass_choices(cls):
        subclass_choices = []
        for name, model in cls.get_subclass_object_choices().items():
            subclass_choices.append((name, model._meta.verbose_name))

        return subclass_choices

    @classmethod
    def get_subclass(cls, name):
        return cls.get_subclass_object_choices().get(name, cls)


class VideoContent(Content):
    codec = models.CharField(max_length=255)


class AudioContent(Content):
    bitrate = models.PositiveIntegerField()


class TextContent(Content):
    text = models.TextField()
