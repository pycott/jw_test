from rest_framework import serializers

from content_pages import models


class VideoContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.VideoContent
        fields = "__all__"


class AudioContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AudioContent
        fields = "__all__"


class TextContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TextContent
        fields = "__all__"


class ContentSerializer(serializers.ModelSerializer):
    CONTENT_CHILD_TYPE_SERIALIZER_MAP = {
        models.VideoContent: VideoContentSerializer,
        models.AudioContent: AudioContentSerializer,
        models.TextContent: TextContentSerializer,
    }

    class Meta:
        model = models.Content
        fields = "__all__"

    def to_representation(self, instance):
        instance = instance.get_child_content()
        serializer_class = self.CONTENT_CHILD_TYPE_SERIALIZER_MAP.get(instance.__class__)
        if serializer_class is None:
            raise NotImplementedError(f'Serializer for content "{instance.__class__.__name__}" is not implemented.')
        return serializer_class(instance=instance).data


class PagesListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='pages-detail', lookup_field='pk')

    class Meta:
        model = models.Page
        fields = "__all__"


class PageDetailSerializer(serializers.ModelSerializer):
    contents = ContentSerializer(many=True, read_only=True)

    class Meta:
        model = models.Page
        fields = "__all__"
