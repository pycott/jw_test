from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from content_pages import models
from content_pages.tasks import increment_counters_for_pages_contents
from . import serializers


class PagesViewSet(ReadOnlyModelViewSet):
    serializer_class_list = serializers.PagesListSerializer
    serializer_class_detail = serializers.PageDetailSerializer
    queryset = models.Page.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return self.serializer_class_list
        if self.action == 'retrieve':
            return self.serializer_class_detail
        raise NotImplementedError(f'serializer class for action "{self.action}" does not specified')

    def get_queryset(self):
        if self.action == 'list':
            return super().get_queryset().prefetch_related("contents")
        if self.action == 'retrieve':
            return super().get_queryset().prefetch_related("contents__audiocontent", "contents__videocontent",
                                                           "contents__textcontent")

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        increment_counters_for_pages_contents.delay(instance.id)
        return Response(serializer.data)
