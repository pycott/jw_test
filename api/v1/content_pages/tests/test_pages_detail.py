from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from content_pages.models import Page, VideoContent
from django_common.testing import TestCaseMixin


class PagesListTestCase(TestCaseMixin, APITestCase):
    API_NAME = 'pages-detail'

    def test_page_detail(self):
        page = Page.objects.create(title="test_page")
        video_content = VideoContent.objects.create(title="test_video", codec="Avi")
        page.contents.add(video_content)

        url = reverse(self.API_NAME, kwargs={"pk": page.pk})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_page_detail_content_structure(self):
        page = Page.objects.create(title="test_page")
        video_content = VideoContent.objects.create(title="test_video", codec="Avi")
        page.contents.add(video_content)

        url = reverse(self.API_NAME, kwargs={"pk": page.pk})
        response = self.client.get(url, format='json')
        data = response.json()
        contents = data["contents"]
        self.assertEqual(len(contents), 1)
        content = contents[0]
        expected_content = {
            "id": video_content.pk,
            "title": video_content.title,
            "counter": video_content.counter,
            "codec": video_content.codec
        }
        self.assertEqual(content, expected_content)
