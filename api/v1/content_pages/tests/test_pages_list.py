from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from content_pages.models import Page, VideoContent
from django_common.testing import TestCaseMixin


class PagesListTestCase(TestCaseMixin, APITestCase):
    API_NAME = 'pages-list'

    def test_page_list(self):
        page = Page.objects.create(title="test_page")
        video_content = VideoContent.objects.create(title="test_video", codec="Avi")
        page.contents.add(video_content)

        url = reverse(self.API_NAME)
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_page_list_has_contents(self):
        page = Page.objects.create(title="test_page")
        video_content = VideoContent.objects.create(title="test_video", codec="Avi")
        page.contents.add(video_content)

        url = reverse(self.API_NAME)
        response = self.client.get(url, format='json')
        data = response.json()["results"][0]
        contents = data["contents"]
        self.assertEqual(len(contents), 1)
