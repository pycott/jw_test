from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'pages', views.PagesViewSet, base_name="pages")

urlpatterns = [
    url(r'^', include(router.urls)),
]
