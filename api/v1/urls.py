from django.urls import path, include

import api.v1.content_pages.urls

urlpatterns = [
    path('content_pages/', include(api.v1.content_pages.urls)),
]
