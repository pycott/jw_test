def getattr_safe(obj, path, default=None):
    for attr in path.split("."):
        obj = getattr(obj, attr, None)
        if obj is None:
            return default
    return default if obj is None else obj
