import base64
import inspect
import logging
import os
import sys

from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.db import transaction
from django.test import override_settings
from django_common.utils import getattr_safe
from rest_framework.test import APIClient

logger = logging.getLogger(__package__)

User = get_user_model()


class TestCaseMixin(object):
    _override = None
    rollback = True
    new_settings = {}
    user_credentials = {
        "username": "admin",
        "password": "123123"
    }

    @classmethod
    def setUpClass(cls):
        cls._module = sys.modules[cls.__module__]
        cls._module._dir = os.path.dirname(cls._module.__file__)
        cls._module._files = os.listdir(cls._module._dir)
        if os.path.basename(cls._module._dir).startswith('test'):
            cls._add_app()
            cls._change_root_url()
        cls._override = override_settings(**cls.new_settings)
        cls._override.enable()
        if 'INSTALLED_APPS' in cls.new_settings:
            cls._migrate()
        s = super(TestCaseMixin, cls)
        if hasattr(s, 'setUpClass'):
            s.setUpClass()
        if cls.rollback:
            cls._set_rollback()

    @classmethod
    def tearDownClass(cls):
        cls._override.disable()
        if 'INSTALLED_APPS' in cls.new_settings:
            cls._migrate()
        s = super(TestCaseMixin, cls)
        if hasattr(s, 'tearDownClass'):
            s.tearDownClass()

    @classmethod
    def _migrate(cls):
        call_command('migrate', verbosity=0)

    @classmethod
    def _add_app(cls):
        if 'models.py' in getattr_safe(cls, '_module._files'):
            installed_apps = [
                'django.contrib.auth',
                'django.contrib.contenttypes',
                'django.contrib.sessions',
                'django.contrib.staticfiles',
            ]
            if 'INSTALLED_APPS' in cls.new_settings:
                installed_apps.extend(cls.new_settings['INSTALLED_APPS'])
            installed_apps.append(getattr(cls, '_module').__package__, )
            cls.new_settings['INSTALLED_APPS'] = installed_apps

    @classmethod
    def _change_root_url(cls):
        if 'urls.py' in getattr_safe(cls, '_module._files'):
            cls.new_settings['ROOT_URLCONF'] = '{package}.urls'.format(package=getattr(cls, '_module').__package__)

    def setUp(self):
        s = super(TestCaseMixin, self)
        if hasattr(s, 'setUp'):
            s.setUp()
        if not hasattr(self, 'cached_client'):
            self.cached_client = self._get_client()
        self.client = self.cached_client

    def _fixture_setup(self):
        pass

    def _fixture_teardown(self):
        pass

    def _get_client(self):
        try:
            auth_user = User.objects.get(username=self.user_credentials["username"])
        except User.DoesNotExist:
            auth_user = User.objects.create(
                username=self.user_credentials["username"],
                password=self.user_credentials["password"],
            )
        auth_string = "%s:%s" % (self.user_credentials['username'], self.user_credentials['password'])
        authorization = 'Basic %s' % base64.encodebytes(auth_string.encode())
        client = APIClient(HTTP_ACCEPT='application/json', HTTP_AUTHORIZATION=authorization)
        client.force_authenticate(user=auth_user)
        logger.debug('Force authentication with username = "%s"' % self.user_credentials['username'])
        return client

    def set_client(self, username, password=None):
        auth_user = User.objects.get(username=username)
        if password:
            auth_string = "%s:%s" % (username, password)
            authorization = 'Basic %s' % base64.encodebytes(auth_string.encode())
            self.client = APIClient(HTTP_ACCEPT='application/json', HTTP_AUTHORIZATION=authorization)
        else:
            self.client = APIClient(HTTP_ACCEPT='application/json')
        self.client.force_authenticate(user=auth_user)

    @classmethod
    def _set_rollback(cls):
        for name, method in inspect.getmembers(cls, predicate=inspect.isfunction):
            if name.startswith('test'):
                setattr(cls, name, cls._rollback(method))

    @staticmethod
    def _rollback(f):
        def wrapper(*args, **kwargs):
            with transaction.atomic():
                result = f(*args, **kwargs)
                transaction.set_rollback(True)
            return result

        return wrapper

    def get_authenticated_user(self):
        return getattr_safe(self, 'client.handler._force_user.username')
