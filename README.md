# Launching

## Run the app

    docker-compose up runserver

## Run the tests

    docker-compose up autotests 

# REST API

The REST API to the example app is described below.

## Get list of Pages

### Request

`GET /api/v1/content_pages/pages/`

    curl -i -H 'Accept: application/json' http://0.0.0.0:8000/api/v1/content_pages/pages/

### Response

    HTTP/1.1 200 OK
    Date: Mon, 06 Aug 2018 21:06:12 GMT
    Server: WSGIServer/0.2 CPython/3.6.2
    Content-Type: application/json
    Vary: Accept, Cookie
    Allow: GET, HEAD, OPTIONS
    X-Frame-Options: SAMEORIGIN
    Content-Length: 1195

    {
        "count": 40,
        "next": "http://0.0.0.0:8000/api/v1/content_pages/pages/?limit=10&offset=10",
        "previous": null,
        "results": [
            {
                "id": 1,
                "url": "http://0.0.0.0:8000/api/v1/content_pages/pages/1/",
                "title": "page0",
                "contents": [
                    3,
                    1,
                    2
                ]
            },
            ...
        ]
    }


## Get list of Pages with pagination

### Request

`GET /api/v1/content_pages/pages/?limit=2&offset=10`

    curl -i -H 'Accept: application/json' http://0.0.0.0:8000/api/v1/content_pages/pages/?limit=2&offset=10

### Response

    HTTP/1.1 200 OK
    Date: Mon, 06 Aug 2018 21:13:15 GMT
    Server: WSGIServer/0.2 CPython/3.6.2
    Content-Type: application/json
    Vary: Accept, Cookie
    Allow: GET, HEAD, OPTIONS
    X-Frame-Options: SAMEORIGIN
    Content-Length: 384

    {
        "count": 40,
        "next": "http://0.0.0.0:8000/api/v1/content_pages/pages/?limit=2&offset=12",
        "previous": "http://0.0.0.0:8000/api/v1/content_pages/pages/?limit=2&offset=8",
        "results": [
            {
                "id": 11,
                "url": "http://0.0.0.0:8000/api/v1/content_pages/pages/11/",
                "title": "page10",
                "contents": [
                    43,
                    44
                ]
            },
            {
                "id": 12,
                "url": "http://0.0.0.0:8000/api/v1/content_pages/pages/12/",
                "title": "page11",
                "contents": [
                    45
                ]
            }
        ]
    }

## Get a specific Page

### Request

`GET /api/v1/content_pages/pages/1/`

    curl -i -H 'Accept: application/json' http://0.0.0.0:8000/api/v1/content_pages/pages/1/

### Response

    HTTP/1.1 200 OK
    Date: Mon, 06 Aug 2018 21:08:57 GMT
    Server: WSGIServer/0.2 CPython/3.6.2
    Content-Type: application/json
    Vary: Accept, Cookie
    Allow: GET, HEAD, OPTIONS
    X-Frame-Options: SAMEORIGIN
    Content-Length: 173
    
    {
        "id": 1,
        "contents": [
            {
                "id": 3,
                "counter": 68,
                "title": "TextContent2",
                "text": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi."
            },
            {
                "id": 1,
                "counter": 68,
                "title": "VideoContent0",
                "codec": "Apple Animation (QuickTime RLE)"
            },
            {
                "id": 2,
                "counter": 68,
                "title": "TextContent1",
                "text": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi."
            }
        ],
        "title": "page0"
    }

## Get a non-existent Page

### Request

`GET /api/v1/content_pages/pages/9999/`

    curl -i -H 'Accept: application/json' http://0.0.0.0:8000/api/v1/content_pages/pages/9999/

### Response

    HTTP/1.1 404 Not Found
    Date: Mon, 06 Aug 2018 21:09:58 GMT
    Server: WSGIServer/0.2 CPython/3.6.2
    Content-Type: application/json
    Vary: Accept, Cookie
    Allow: GET, HEAD, OPTIONS
    X-Frame-Options: SAMEORIGIN
    Content-Length: 23
    
    {"detail":"Not found."}
